package org.weimdall.NotesENT.Model;

import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class NotesModel extends Observable
{
	private static final Logger	logger	= LoggerFactory.getLogger(IdentificationModel.class);
	
	
	public class Parseur implements ContentHandler
	{
		private class Devoir<String, V> extends HashMap<String, V> implements Comparable
		{
			@Override
			public int compareTo(Object o)
			{
				Devoir d = (Devoir)o;
				java.lang.String str1 = (java.lang.String)this.get("Date");
				java.lang.String str2 = (java.lang.String)d.get("Date");
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				try
				{
					Date d1 = dateFormat.parse(str1);
					Date d2 = dateFormat.parse(str2);
					return d1.compareTo(d2);
				} catch (ParseException e)				
				{
					return 0;
				}
			}
			
						
		}
		private boolean info = false;
		private boolean devoir = false;
		
		private String nom = null;
		private String prenom = null;
		private ArrayList<Devoir<String, String>> devoirs = new ArrayList<Devoir<String, String>>();
		private Map<String, String> devoirCourant;
		private String champCourant;
		
		public String getNom()
		{
			return nom;
		}
		public String getPrenom()
		{
			return prenom;
		}
		public ArrayList<Devoir<String, String>> getDevoirs()
		{
			return devoirs;
		}
		
		@Override
		public void characters(char[] arg0, int arg1, int arg2) throws SAXException
		{
			if(info)
			{
				if(nom == null)
					nom = new String(arg0).substring(arg1, arg1+arg2);
				else
					prenom = new String(arg0).substring(arg1, arg1+arg2);
			}
			else if(devoir)
				devoirCourant.put(champCourant, new String(arg0).substring(arg1, arg1+arg2));
				
			

		}

		@Override
		public void endDocument() throws SAXException
		{
			Collections.sort(devoirs);
		}

		@Override
		public void endElement(String arg0, String arg1, String arg2) throws SAXException
		{
			if(arg1.equals("Devoir"))
			{
				devoirs.add((Devoir<String, String>)devoirCourant);
				devoir = false;
			}
			else if(arg1.equals("Info"))
				info = false;
			
		}

		@Override
		public void endPrefixMapping(String arg0) throws SAXException
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void ignorableWhitespace(char[] arg0, int arg1, int arg2) throws SAXException
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void processingInstruction(String arg0, String arg1) throws SAXException
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setDocumentLocator(Locator arg0)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void skippedEntity(String arg0) throws SAXException
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void startDocument() throws SAXException
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void startElement(String arg0, String arg1, String arg2, Attributes arg3) throws SAXException
		{
			if(arg1.equals("Devoir"))
			{
				devoir = true;
				devoirCourant = new Devoir<String, String>();
				devoirCourant.put("nb", arg3.getValue("nb"));
				devoirCourant.put("min", arg3.getValue("min"));
				devoirCourant.put("max", arg3.getValue("max"));
				devoirCourant.put("avg", arg3.getValue("avg"));
			}
			else if(arg1.equals("Info"))
				info = true;
			if(devoir)
				champCourant = new String(arg1);
			
		}

		@Override
		public void startPrefixMapping(String arg0, String arg1) throws SAXException
		{
			// TODO Auto-generated method stub
			
		}
		
	}
	private Parseur parseur = new Parseur();
	private XMLReader reader = null;
	
	
	public NotesModel(String token) throws Exception
	{
		URL urlIdentification;
		URLConnection yc;
		try	
		{
			urlIdentification = new URL("http://ketchzai.ovh/API/?format=xml&token="+token);
			yc = urlIdentification.openConnection();
			
		} catch (Exception e)
		{
			logger.error("Impossible le lire le XML.");
			throw new Exception("Service inaccessible");
		}
		
		try{
			reader = XMLReaderFactory.createXMLReader();
		} catch (SAXException e1){
			logger.error("Erreur lors de la création du XMLReader");
		}
		
		reader.setContentHandler(parseur);
		
		try{
			reader.parse(new InputSource(yc.getInputStream()));
		}catch (Exception e){
			logger.error("Impossible de parser le fichier.");
		}
		
	}
	
	public Map<String, String> getDevoirs(int i)
	{
		return this.parseur.getDevoirs().get(i-1);
	}
	public ArrayList<String> getMatieres()
	{
		ArrayList<String> liste = new ArrayList<String>();
		for(int i = 1 ; i <= this.nbDevoirs() ; i++)
		{
			String temp = this.getDevoirs(i).get("Matiere");
			if(!liste.contains(temp))
				liste.add(temp);
			
		}
		return liste;
	}
	
	public int nbDevoirs()
	{
		return this.parseur.devoirs.size();
	}
	
}
