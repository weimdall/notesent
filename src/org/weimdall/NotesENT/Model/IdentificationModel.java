package org.weimdall.NotesENT.Model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdentificationModel
{
	private static final Logger	logger	= LoggerFactory.getLogger(IdentificationModel.class);
	private String token = new String();
	
	public boolean identifie(String username, String password)
	{
		URL urlIdentification;
		this.token = "0";
		try
		{
			urlIdentification = new URL("http://ketchzai.ovh/API/?username="+username+"&password="+password);
			URLConnection yc = urlIdentification.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
			this.token = new String(in.readLine());
			in.close();
			
		} catch (Exception e)
		{
			logger.error("Impossible d'atteindre le service d'identification.");
			return false;
		}
		
		logger.debug("Réponse d'Aurion :" + this.token);
		
		return this.token.startsWith("e_");
		
	}
	
	public String getToken()
	{
		return token;
	}
	
	
}
