package org.weimdall.NotesENT;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.weimdall.NotesENT.Model.IdentificationModel;
import org.weimdall.NotesENT.Model.NotesModel;
import org.weimdall.NotesENT.View.GraphsView;
import org.weimdall.NotesENT.View.IdentificationView;
import org.weimdall.NotesENT.View.LoadingView;
import org.weimdall.NotesENT.View.NotesView;
import org.weimdall.NotesENT.View.FX.FxApplication;

public class Controller implements Observer
{
	private static final Logger	LOGGER	= LoggerFactory.getLogger(Controller.class);
	
	private FxApplication fxApp;	
	private IdentificationView idView;
	private NotesView noteView;
	private GraphsView graphsView;
	private IdentificationModel idModel;
	private NotesModel mNotes;
	
	@SuppressWarnings("static-access")
	public Controller()
	{
		
		fxApp = new FxApplication();
		try {
			CountDownLatch latch = new CountDownLatch(1);
			new Thread(new Runnable() {
				@Override
				public void run() {
					FxApplication.Launch(latch);
				}
			}).start();
			latch.await();		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		fxApp.getInstance().addObserver(this);
		
		idModel = new IdentificationModel();
		idView = new IdentificationView(fxApp.getInstance());
		fxApp.setView(idView);
		fxApp.display();
		
		
	}
	
	
	public static void main(String[] args)
	{
		new Controller();
	}
	@Override
	public void update(Observable o, Object arg)
	{
		if(arg instanceof ArrayList)
		{
			ArrayList<String> tab = (ArrayList<String>)arg;
			if(tab.get(0).equals(idView.getName()))			// Si le signal vient de la vue idView on tente une identification via idModel
			{
				fxApp.setView(new LoadingView());
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						if(idModel.identifie(tab.get(1), tab.get(2)))
						{
							try
							{
								mNotes = new NotesModel(idModel.getToken());
							} catch (Exception e)
							{
								LOGGER.error("Erreur critique.");
								System.exit(1);
							}
							
							noteView = new NotesView(fxApp.getInstance(), mNotes);
							graphsView = new GraphsView(fxApp.getInstance(), mNotes);
							fxApp.setView(noteView);
								
						}
						else
						{
							LOGGER.error("Impossible d'authentifier l'utilisateur sur Aurion.");
							fxApp.setView(idView);
						}
					}
						
				}).start();
			}
			else if(tab.get(0).equals(noteView.getName()))	
			{
				if(tab.get(1).equals("Graphs"))
					fxApp.setView(graphsView);
				else if(tab.get(1).equals("Quitter"))
					fxApp.close();
			}
			else if(tab.get(0).equals(graphsView.getName()))	
				if(tab.get(1).equals("Notes"))
					fxApp.setView(noteView);
				else if(tab.get(1).equals("Quitter"))
					fxApp.close();
			
		}
	}

	

}
