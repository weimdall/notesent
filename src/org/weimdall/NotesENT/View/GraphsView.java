package org.weimdall.NotesENT.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.weimdall.NotesENT.Model.NotesModel;
import org.weimdall.NotesENT.View.FX.FxApplication;
import org.weimdall.NotesENT.View.FX.FxView;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class GraphsView implements FxView
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GraphsView.class);
	
	private static class FxGraph
	{
		private final Font font = Font.font("Droid Sans", 15);
		private int minX;
		private int maxX;
		private NumberAxis xAxis = new NumberAxis();
        private NumberAxis yAxis = new NumberAxis();
        private AreaChart<Number,Number> graph;
		private static double sizeY;
		private XYChart.Series serie, serieAVG;
		private String matiere;
		private double somme, nb, moyenne, lastMark;
		
		public FxGraph(int min, int max, String m)
		{
			this.matiere = m;
			this.minX = -max;
			this.maxX = -min;
			this.somme = 0;
			this.nb = 0;
			this.moyenne = 0;
			serie = new XYChart.Series<Number, Number>();
			serieAVG = new XYChart.Series<Number, Number>();
			xAxis = new NumberAxis(minX, maxX, 10);
	        yAxis = new NumberAxis(0,20,10);
	        xAxis.setAutoRanging(false);
	        xAxis.setTickLabelsVisible(false);
	        yAxis.setAutoRanging(false);
	        

            graph = new AreaChart<Number,Number>(xAxis,yAxis);
			graph.setTitle(matiere);
			graph.setLegendVisible(false);
			graph.setPrefWidth(1000);
		}
		public void add(int n1, double n2)
		{
			somme+=n2;
			nb++;
			moyenne=somme/nb;
			if(serie.getData().size() == 0)
				serie.getData().add(new XYChart.Data(minX-100, n2));
			serie.getData().add(new XYChart.Data(-n1, n2));
			lastMark = n2;
		}
		public void flush()
		{
			serieAVG.getData().clear();
			serieAVG.getData().add(new XYChart.Data(minX, moyenne));
			serieAVG.getData().add(new XYChart.Data(maxX, moyenne));
			serie.getData().add(new XYChart.Data(maxX+100, lastMark));
			graph.getData().clear();
			graph.getData().addAll(serie, serieAVG);
		}
		public AreaChart<Number, Number> getGraph()
		{
			return graph;
		}
		public String getMatiere()
		{
			return matiere;
		}
		public Font getFont()
		{
			return font;
		}
	}
	private FxApplication fxApp;
	private Image imgNote;
	private ArrayList<FxGraph> graphs;
	private VBox vbox, vBoxGraph;
	private ScrollPane scroll;
	private Scene scene;

	private ToolBar toolbar = null;
	
	private NotesModel model;
	
	public GraphsView(FxApplication app, NotesModel pModel)
	{
		graphs = new ArrayList<FxGraph>();
		model = pModel;
		model.nbDevoirs();
		fxApp = app;
		vbox = new VBox();
		vBoxGraph = new VBox();
		scroll = new ScrollPane();
		imgNote = new Image("./res/note.png", 50, 50, true, true);
		Button q = new Button("Quitter");
		q.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event)
			{
				ArrayList<String> tab = new ArrayList<String>();
            	tab.add(getName());
            	tab.add(q.getText());
            	fxApp.notifyObersers(tab);
			}
			
		});
		Button s = new Button("Notes");
		s.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event)
			{
				ArrayList<String> tab = new ArrayList<String>();
            	tab.add(getName());
            	tab.add(s.getText());
            	fxApp.notifyObersers(tab);
			}
			
		});
		toolbar = new ToolBar(s, q);
		vbox.getChildren().add(toolbar);
		vbox.getChildren().add(scroll);
		scroll.setContent(vBoxGraph);
		
		ArrayList<String> matiere = model.getMatieres();
		Date max, min;
		max = null;
		min = null;
		for(int i = 1 ; i <= model.nbDevoirs() ; i++) // Recherche date min et max
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			try
			{
				Date date = dateFormat.parse(model.getDevoirs(i).get("Date"));
				if(min == null || date.before(min))
					min = date;
				if(max == null || date.after(max))
					max = date;
			} catch (ParseException e)				
			{
				LOGGER.error("Erreur lors du parsage de la date de " + model.getDevoirs(i).get("Code"));
			}
		}
		int deltaDays = (int)TimeUnit.MILLISECONDS.toDays(max.getTime()-min.getTime());
		for(int i = 0 ; i < matiere.size() ; i++)
			graphs.add(new FxGraph(-2, deltaDays+2, matiere.get(i)));
		
		for (int j = 1 ; j <= model.nbDevoirs() ; j++)
		{
			for(int i = 0 ; i < graphs.size() ; i++)
			{
				if(graphs.get(i).getMatiere().equals(model.getDevoirs(j).get("Matiere")))
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					try
					{
						Date date = dateFormat.parse(model.getDevoirs(j).get("Date"));
						int days = (int)TimeUnit.MILLISECONDS.toDays(max.getTime()-date.getTime());
						graphs.get(i).add(days, Double.parseDouble(model.getDevoirs(j).get("Note")));
					} catch (ParseException e)
					{
						LOGGER.error("Erreur lors du parsage de la date de " + model.getDevoirs(j).get("Code"));
					}
				}
			}
		}	
		for(int i = 0 ; i < graphs.size() ; i++)
		{
			graphs.get(i).flush();
			vBoxGraph.getChildren().add(graphs.get(i).getGraph());
		}
		
		final ChangeListener<Number> listener = new ChangeListener<Number>()
		{
		  @Override
		  public void changed(ObservableValue<? extends Number> observable, Number oldValue, final Number newValue)
		  {
		    Scaling();
		  }
		};
		fxApp.getInstance().getStage().widthProperty().addListener(listener);
		fxApp.getInstance().getStage().heightProperty().addListener(listener);

        this.scene = new Scene(vbox, 300, 275);
	}
	
	
	@Override
	public Scene getScene()
	{	
		fxApp.getInstance().getStage().setWidth(1024);
		fxApp.getInstance().getStage().setHeight(500);
		Scaling();
        return this.scene;
	}
	
	public void Scaling()
	{
		for(int i = 0 ; i < graphs.size() ; i++)
			graphs.get(i).getGraph().setPrefWidth(fxApp.getStage().getWidth()-24);
	}

	@Override
	public String getName()
	{
		return new String("Graphs");
	}

	@Override
	public void update(Observable o, Object arg)
	{
		
	}

}
