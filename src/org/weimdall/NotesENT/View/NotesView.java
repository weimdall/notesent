package org.weimdall.NotesENT.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.weimdall.NotesENT.Model.NotesModel;
import org.weimdall.NotesENT.View.FX.FxApplication;
import org.weimdall.NotesENT.View.FX.FxView;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Popup;

public class NotesView implements FxView
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NotesView.class);
	
	private class FxNote
	{
		public static final double sizeX = 50;
		public static final double sizeY = 50;
		private static final double sizeFont = 15;
		private final Font font = Font.font("Droid Sans", sizeFont);
		private double x;
		private double y;
		private Image img;
		private Map<String, String> infos;
		private Date date;
		
		public FxNote(Image i, double pX, double pY, Map<String, String> map)
		{
			this.x = pX;
			this.y = pY;
			this.img = i;
			this.infos = map;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			try
			{
				date = dateFormat.parse(map.get("Date"));
			} catch (ParseException e)
			{
				LOGGER.error("Erreur lors du parsage de la date de " + map.get("Code"));
			}
		}
		
		public Map<String, String> getInfos()
		{
			return infos;
		}
		
		public void Draw(GraphicsContext context)
		{
			context.drawImage(img, x, y);
			context.setFont(font);
			context.setTextAlign(TextAlignment.CENTER);
			context.fillText(infos.get("Note"), x+(sizeX/2), y+(sizeY/2)+(sizeFont/3));
		}
		public void Move(double deltaX, double deltaY)
		{
			x+=deltaX;
			y+=deltaY;
		}
		public Date getDate()
		{
			return date;
		}
		public void setPos(double pX, double pY)
		{
			x=pX;
			y=pY;
		}
		public double getX()
		{
			return x;
		}
		public double getY()
		{
			return y;
		}
		public Font getFont()
		{
			return font;
		}
		public String toString()
		{
			return new String("   " + infos.get("Nom") + "   \n"
								+ "   Date : " + infos.get("Date") + "   \n" 
								+ "   Moyenne : " + infos.get("avg") + "   \n"
								+ "   Maximum : " + infos.get("max") + "   \n"
								+ "   Minimum : " + infos.get("min") + "   ");
		}
	}
	private FxApplication fxApp;
	private Image imgNote;
	private ArrayList<FxNote> notes;
	private VBox vbox;
	private Scene scene;
	
	private Canvas canvas;
	private GraphicsContext context;
	private Popup popup;
	
	private ToolBar toolbar = null;
	
	private NotesModel model;
	
	public NotesView(FxApplication app, NotesModel pModel)
	{
		notes = new ArrayList<FxNote>();
		canvas = new Canvas(1024-24, 500);
		context = canvas.getGraphicsContext2D();
		model = pModel;
		model.nbDevoirs();
		model.addObserver(this);
		fxApp = app;
		vbox = new VBox();
		imgNote = new Image("./res/note.png", 50, 50, true, true);
		popup = new Popup();
		Button q = new Button("Quitter");
		q.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event)
			{
				ArrayList<String> tab = new ArrayList<String>();
            	tab.add(getName());
            	tab.add(q.getText());
            	fxApp.notifyObersers(tab);
			}
			
		});
		Button s = new Button("Graphs");
		s.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event)
			{
				ArrayList<String> tab = new ArrayList<String>();
            	tab.add(getName());
            	tab.add(s.getText());
            	fxApp.notifyObersers(tab);
			}
			
		});
		toolbar = new ToolBar(s,q);
		
		for(int i = 1 ; i <= model.nbDevoirs() ; i++)
			notes.add(new FxNote(imgNote, (i-1)*(FxNote.sizeX*1.5), 0, model.getDevoirs(i))); //Positionnement temporaire en ligne
		
		this.Draw();
		
		canvas.setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event)
			{
				boolean onMark = false;
				for(int i = 0 ; i < notes.size() ; i++)
				{
					FxNote note = notes.get(i);
					if(note.getX() < event.getX() && note.getX()+FxNote.sizeX > event.getX() && 
							note.getY() < event.getY() && note.getY()+FxNote.sizeY > event.getY())
					{
						onMark = true;
						popup.setX(event.getX()+app.getStage().getX());
						popup.setY(event.getY()-50+app.getStage().getY());
						if(!popup.isShowing())
						{
							popup.getContent().clear();
							Text t = new Text("\n" + note.toString());
							t.setFont(note.getFont());
							Rectangle r = new Rectangle(t.getBoundsInLocal().getWidth(), t.getBoundsInLocal().getHeight(), Color.GHOSTWHITE);
							r.setEffect(new GaussianBlur(25));
							popup.getContent().addAll(r, t);
							popup.show(app.getStage());
						}
					}
				}
				if(popup.isShowing() && !onMark)
					popup.hide();
			}
			
		});
		
		final ChangeListener<Number> listener = new ChangeListener<Number>()
		{
		  @Override
		  public void changed(ObservableValue<? extends Number> observable, Number oldValue, final Number newValue)
		  {
		    Scaling();
		  }
		};
		fxApp.getInstance().getStage().widthProperty().addListener(listener);
		fxApp.getInstance().getStage().heightProperty().addListener(listener);

		vbox.getChildren().add(toolbar);
		ScrollPane sp = new ScrollPane();
		sp.setContent(canvas);
		vbox.getChildren().add(sp);
        this.scene = new Scene(vbox, 300, 275);
	}
	
	public void Draw()
	{
		ArrayList<String> matiere = model.getMatieres();
		int ecartLigne = (int)(FxNote.sizeX*1.5);
		context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		
		for(int i = 0 ; i < notes.size() ; i++)
		{
			notes.get(i).setPos(notes.get(i).getX(), (matiere.indexOf(notes.get(i).getInfos().get("Matiere"))*ecartLigne)+FxNote.sizeX/4);
			notes.get(i).Draw(context);
		}
		
		context.applyEffect(new DropShadow(5, 5, 0, Color.GRAY));
		context.applyEffect(new DropShadow(5, 0, 5, Color.GREY));
			
		context.setStroke(Color.GREY);
		context.setLineWidth(3);
		context.setFont(notes.get(0).getFont());
		
		int j = ecartLigne;
		for(int i = 0 ; i < matiere.size() ; i++)
		{
			context.fillText(matiere.get(i), 30, (i*ecartLigne) + FxNote.sizeY);
			context.strokeLine(0, j, canvas.getWidth(), j);
			j+=ecartLigne;
		}
		
		
		
	}
	
	@Override
	public Scene getScene()
	{	
		fxApp.getInstance().getStage().setWidth(1024);
		fxApp.getInstance().getStage().setHeight(500);
		Scaling();
        return this.scene;
	}
	
	public void Scaling()
	{
		canvas.setWidth(fxApp.getInstance().getStage().getWidth()-24);
		canvas.setHeight(fxApp.getInstance().getStage().getHeight());
		Date max, min;
		max = null;
		min = null;
		for(int i = 0 ; i < notes.size() ; i++)
		{
			if(min == null || notes.get(i).getDate().before(min))
				min = notes.get(i).getDate();
			if(max == null || notes.get(i).getDate().after(max))
				max = notes.get(i).getDate();
		}
		int deltaDays = (int)TimeUnit.MILLISECONDS.toDays(max.getTime()-min.getTime());
		
		if(deltaDays != 0)
		{
			int chronoScale = (int)((canvas.getWidth()-FxNote.sizeX-100)/deltaDays);
			
			for(int i = 0 ; i < notes.size() ; i++)
			{
				deltaDays = (int)TimeUnit.MILLISECONDS.toDays(notes.get(i).getDate().getTime()-min.getTime());
				notes.get(i).setPos((deltaDays*chronoScale)+100, notes.get(i).getY());
			}
		}
		this.Draw();
	}

	@Override
	public String getName()
	{
		return new String("Notes");
	}

	@Override
	public void update(Observable o, Object arg)
	{
		
	}

}
