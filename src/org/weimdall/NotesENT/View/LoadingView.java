package org.weimdall.NotesENT.View;

import java.util.Observable;

import org.weimdall.NotesENT.View.FX.FxView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.GridPane;

public class LoadingView implements FxView
{

	@Override
	public String getName()
	{
		return new String("Chargement");
	}

	@Override
	public Scene getScene()
	{
		GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
		ProgressIndicator pb = new ProgressIndicator();
		grid.add(pb,  0,  0);
        return new Scene(grid, 300, 275);
	}

	@Override
	public void update(Observable o, Object arg)
	{
		// TODO Auto-generated method stub
		
	}

}
