package org.weimdall.NotesENT.View;

import java.util.ArrayList;
import java.util.Observable;

import org.weimdall.NotesENT.View.FX.FxApplication;
import org.weimdall.NotesENT.View.FX.FxView;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class IdentificationView implements FxView
{
	private FxApplication fxApp;
	private Button btn;
	private Label pw;
	private Text scenetitle;
	private Label username;
	private TextField usernameField;
	private PasswordField pwField;
	private GridPane grid;
	private HBox hbBtn;
	private Scene scene;
	
	public IdentificationView(FxApplication app)
	{
		this.fxApp = app;
		this.btn = new Button("Connexion");
		this.pw = new Label("Mot de passe:");
		this.scenetitle = new Text("Bienvenue");
		this.username = new Label("Nom d'utilisateur:");
		this.usernameField = new TextField();
		this.pwField = new PasswordField();
		this.grid = new GridPane();
		this.hbBtn = new HBox(10);
		grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        grid.add(username, 0, 1);
        grid.add(usernameField, 1, 1);
        grid.add(pw, 0, 2);
        grid.add(pwField, 1, 2);
        
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);
        
        btn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e) {
            	ArrayList<String> tab = new ArrayList<String>();
            	tab.add(getName());
            	tab.add(usernameField.getText());
            	tab.add(pwField.getText());
            	fxApp.notifyObersers(tab);
            }
        });
        
        this.scene = new Scene(grid, 300, 275);
	}

	@Override
	public Scene getScene()
	{	
		this.pwField.setText("");
		fxApp.getInstance().getStage().setWidth(400);
		fxApp.getInstance().getStage().setHeight(200);
        return this.scene;
	}

	@Override
	public String getName()
	{
		return new String("Identification");
	}

	@Override
	public void update(Observable o, Object arg)
	{
		// TODO Auto-generated method stub
		
	}

}
