package org.weimdall.NotesENT.View.FX;

import java.util.Observer;
import javafx.scene.Scene;

public interface FxView extends Observer
{
	public String getName();
	public Scene getScene();
}
