/*
 * @(#)FieldJavaFxView.java
 * 
 * Copyright (c) 2015 FXLABS and/or its affiliates.
 * All rights reserved. Use is subject to license terms.
 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the distribution.
 *  - Neither the name of Oracle nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.weimdall.NotesENT.View.FX;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;


public class FxApplication extends Application
{
	private class RelaiController extends Observable
	{
		public void Refresh(ArrayList<String> tab)
		{
			LOGGER.debug("Notify to : "+this.countObservers() + " observers.");
			setChanged();
			notifyObservers(tab);
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(FxApplication.class);
	private static FxApplication instance = null;
	private static CountDownLatch latch;
	
	public static void Launch(CountDownLatch countDownLatch)
	{
		latch = countDownLatch;
		FxApplication.launch();
	}

	public static FxApplication getInstance()
	{
		return instance;
	}
	
	public void notifyObersers(ArrayList<String> tab)
	{
		this.relaisController.Refresh(tab);
	}
	

	private Stage stage = null;
	private RelaiController relaisController;
	
	public void addObserver(Observer o)
	{
		this.relaisController.addObserver(o);
	}
	

	public void display()
	{
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				getInstance().getStage().show();
			}
		});
	}

	public void close()
	{
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				getInstance().getStage().close();
			}
		});
	}
	
	public void setView(FxView scene)
	{
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				getInstance().getStage().setTitle(scene.getName() + " - My Mark");
				getInstance().getStage().setScene(scene.getScene());
			}
		});
	}

	
	@Override
	@SuppressWarnings("static-access")
	public void start(Stage primaryStage) throws Exception
	{
		this.instance = this;
		relaisController = new RelaiController();
		this.stage = primaryStage;
		
        primaryStage.setTitle("My Mark"); 
//        primaryStage
        
        if (latch!=null)
        	latch.countDown();
	}
	
	public Stage getStage()
	{
		return this.stage;
	}

	

}
